# Sample C++ project for CI/CD on Gitlab

Based on [CI/CD templates](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/C++.gitlab-ci.yml) provided by Gitlab, this C++ project aims to:

* Provide cross-platform software using CMake
* Run unit test using Google Test framework
* Ship Docker image to Gitlab Container Registry
* Publish software documentation to Gitlab Pages

## To go futher:
- A great [C++ project](https://gitlab.com/b110011/cpp-template-project) with some tools like clang compiler, cppcheck, ...

## References
- [Unit test demo](https://github.com/bast/gtest-demo)
- [Docker Image build](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml)
- [Doxygen configuration](https://gitlab.com/pages/doxygen)